## Summary

(Summarize the bug in few words)

## Open Data Hub version

(Add the Open Data Hub version you use to reproduce this issue. Don't forget to attach the CR file you used to reproduce the bug)

## OpenShift version

(Add the OpenShift version you use to reproduce this issue)

## Steps to reproduce

(Describe precisely all steps to reproduce the bug)

## Reproducer code (if applicable)

(Send a pastebin or a gist or a gitlab snippet link with a reproducer code)

## Workaround (if applicable)

(Describe the workaround found if applicable)

## Other information

(Paste any other relevant information = please use code blocks (```) to format console output and logs as it's very hard to read otherwise)

/label ~bug
/cc @jnakfour @LaVLaS @vpavlin @crobby @pdmack